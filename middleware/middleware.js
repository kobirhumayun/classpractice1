const data = require('../data/dataBase')
// methode GET
// url "/"
const home = (req, res)=>{

    res.status(200).send("Home Page")
}
// methode GET
// url "/:id"
const proId = (req, res)=>{
    const {id} = req.params
    const filterData = data.filter(e => {
        return e.id == id
    })
    if(filterData.length){
        res.status(200).send(filterData[0])
    }else return res.status(400).send("No Product Fund")
}

// methode GET
// url "/category/q?"
const category = (req, res)=>{
    console.log(req.query.category);
    const category = req.query.category
    const filterData = data.filter(e => {
        return e.category == category
    })
    if(filterData.length){
        res.status(200).send(filterData)
    }else return res.status(400).send("No Product Fund")
}

// methode GET
// url "*"
const  routsNotexist = (req, res)=>{
    res.status(404).send("API not found")
}


module.exports = {
    home,
    proId,
    routsNotexist,
    category
}