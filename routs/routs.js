const express = require('express')
const routs = express.Router()
const {
    home,
    proId,
    routsNotexist,
    category
} = require('../middleware/middleware')

// root url
routs.get('/', home)
// sample url (/1)
routs.get("/:id", proId)
// sample url (/category/q?category=jewelery)
routs.get("/category/q", category)



routs.get("*", routsNotexist)
module.exports = routs